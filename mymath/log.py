import math
from mymath.simple_fun import SimpleFun

class Log(SimpleFun):

    def setUp(self):
        self.afact =(math.exp(self.y1)-math.exp(self.y0))/(self.x1-self.x0)
        self.bfact =math.exp(self.y0)-(self.afact*self.x0)

    def y(self,x):
        return (math.log(self.afact*x+self.bfact))

    
