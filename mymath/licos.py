import math
from mymath.simple_fun import SimpleFun

class Licos(SimpleFun):

    def setUp(self):
        self.afact = ((self.y1-self.y0)-math.cos(self.x1)*math.cos(math.pi*self.x1)+math.cos(self.x0)*math.cos(math.pi*self.x0))/(self.x1-self.x0)
        self.bfact = self.y0-(self.afact*self.x0)-math.cos(self.x0)*math.cos(math.pi*self.x0)

    def y(self,x):

        return (x*self.afact)+self.bfact+(math.cos(x)*math.cos(math.pi*x))
####
#implementare possibilità di scegliere ogni quante componenti sia il ciclo dell'onda
#trovando la formuLA della variabile da moltiplicare al coseno: cos(n*x) per ogni caso
####
