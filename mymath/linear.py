from mymath.simple_fun import SimpleFun

class Linear(SimpleFun):

    def setUp(self):
        self.afact = (self.y1-self.y0)/(self.x1-self.x0)
        self.bfact = self.y0-(self.afact*self.x0)

    def y(self,x):
        return (self.afact*x)+self.bfact
