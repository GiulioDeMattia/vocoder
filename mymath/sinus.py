import math
from mymath.simple_fun import SimpleFun

class Sinus(SimpleFun):

    def setUp(self):
        self.w=2.0*math.pi*2.0
        self.amp =(self.y1-self.y0)/2.0
        self.phase = math.asin(self.w*self.x0)

    def y(self,x):
        return self.amp*math.sin(self.w*x+self.phase)

    
