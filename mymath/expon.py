import math
from mymath.simple_fun import SimpleFun

class Expon(SimpleFun):

    def setUp(self):
        self.afact =(math.log(self.y1)-math.log(self.y0))/(self.x1-self.x0)
        self.bfact =math.log(self.y0)-(self.afact*self.x0)

    def y(self,x):
        return math.exp(self.afact*x+self.bfact)
