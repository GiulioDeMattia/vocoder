from mymath.floatrange import frange
from mymath.simple_fun  import SimpleFun
from mymath.linear  import Linear
from mymath.expon   import Expon
from mymath.log     import Log
from mymath.licos   import Licos
from mymath.sinus   import Sinus
