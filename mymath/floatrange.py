import decimal

def frange(start,stop,step=1.0):
    while start < stop:
        yield float(start)
        start += decimal.Decimal(step)
