import yaml
from strumentoforma import Strumentoforma
class Contesto:

    @classmethod
    def create(cls,file):
        res = None
        dict = None
        with open(file) as f:
            dict = yaml.load(f,Loader=yaml.Loader)
        names = [k for k,v in dict.items()]
        res = [Strumentoforma(n,dict[n]) for n in names]
        return res
