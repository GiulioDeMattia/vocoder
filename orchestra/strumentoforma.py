import yaml
from istanzaforma import Istanzaforma
from oggettosonoro import OggettoSonoro

class Strumentoforma:
    def __init__(self,nome,dict):
        self.istanze=[]
        self.oggetti_sonori=[]
        self.setUp(nome,dict)

    def setUp(self,nome,dict):
        nome_file="metapartitura/"+str(nome)+".yaml"
        self.istanze=self.getInstances(nome_file)
        self.oggetti_sonori=OggettoSonoro.create(dict)

    def getInstances(self,file):
        res = None
        dict = None
        with open(file) as f:
            dict = yaml.load(f,Loader=yaml.Loader)
        names = [k for k,v in dict.items()]
        res = [Istanzaforma(n,dict[n]) for n in names]
        return res

    def to_csound(self):
        i='\n'.join([i.to_csound() for i in self.istanze])
        o='\n'.join([o.to_csound() for o in self.oggetti_sonori])
        return i+o
