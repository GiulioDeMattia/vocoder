import yaml
from oggettosonoro import OggettoSonoro
import pdb

class Suonoforma(Nucleo):
    def __init__(self,file):
        super().__init__(at,dur)
        self.setUp(file)


    def setUp(self,file):
        self.Oggettisonori = OggettoSonoro.create(file)


    def to_csound(self):
        return '\n'.join([c.to_csound() for c in self.Oggettisonori])
