import pdb
import yaml
class OggettoSonoro:

    def __init__(self,nome):
        self.nome=nome

    def to_csound(self):
        return "%s\n"%(self.nome)


    @classmethod
    def create(cls,dict):
        res = None
        names = [v for k,v in dict.items()]
        res = [OggettoSonoro(n) for n in names]
        return res
