import sys
sys.path.append('.')
sys.path.append('..')
sys.path.append('../mymath')
from mymath import *
import matplotlib.pyplot as mpl
class MyMathPlot:

    def __init__(self):
        self.x0 = 0
        self.x1 = 10
        self.y0 = 1
        self.y1 = 3
        self.x = [x for x in frange(self.x0,self.x1,.01)]

    def plot_licos(self):
        f = Licos(self.x0,self.y0,self.x1,self.y1)
        y = [f.y(x) for x in self.x]
        mpl.plot(self.x,y)
        mpl.savefig("plots/licos_plot.png")

    def plot_linear(self):
        f = Linear(self.x0,self.y0,self.x1,self.y1)
        y = [f.y(x) for x in self.x]
        mpl.plot(self.x,y)
        mpl.savefig("plots/linear_plot.png")

    def plot_expon(self):
        f = Expon(self.x0,self.y0,self.x1,self.y1)
        y = [f.y(x) for x in self.x]
        mpl.plot(self.x,y)
        mpl.savefig("plots/expon_plot.png")

    def plot_log(self):
        f = Log(self.x0,self.y0,self.x1,self.y1)
        y = [f.y(x) for x in self.x]
        mpl.plot(self.x,y)
        mpl.savefig("plots/log_plot.png")

    def plot_sinus(self):
        f = Sinus(self.x0,self.y0,self.x1,self.y1)
        y = [f.y(x) for x in self.x]
        mpl.plot(self.x,y)
        mpl.savefig("plots/sinus_plot.png")

if __name__ == '__main__':
    mp = MyMathPlot()
    mp.plot_licos()
    mp.plot_linear()
    mp.plot_expon()
    mp.plot_sinus()
    mp.plot_log()
   
