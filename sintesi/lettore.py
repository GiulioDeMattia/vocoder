from inviluppo import Inviluppo
import pdb

class Lettore:
    def __init__(self ,file,num_inv,type_envs):
        file_rett= file[1:-1]
        self.file="../analisi/"+file_rett
        self.size=131072
        self.inviluppi=[]
        self.create(type_envs,num_inv)

    def create(self,type_envs,num_inv):
        for n in range(num_inv):
            self.inviluppi.append(Inviluppo(n+2,type_envs))
        f=open(self.file,"r")
        for line in f:
            (time, cols) = line.split(None, 1)
            time = float(time)
            col_list = cols.split(None)
            for c in range(len(col_list)):
                self.inviluppi[c].add_coord(time, float(col_list[c]))


    def to_csound(self):
        return '\n'.join([inv.to_csound(self.size) for inv in self.inviluppi])


