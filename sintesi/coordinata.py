class Coordinata:
    def __init__(self, tempo, amp):
        self.tempo = tempo
        self.ampiezza = amp
    def toString(self):
        return "%8.4f %12.8f" % (self.tempo, self.ampiezza)
    def to_csound(self):
        return "%12.8f %d "%(self.ampiezza,self.tempo)
