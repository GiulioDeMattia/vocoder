import pdb
from instr import *
from lettore import Lettore

class Scrittore:
    def __init__(self,file):
        self.instr=[]
        self.data=[]

    def setUp(self,file):
        f=open(file,"r")
        for line in f:
            self.data=line.split()
        self.freq=float(self.data[4])
        self.n_oct=int(self.data[5])
        self.flt_x_oct=int(self.data[6])


    def to_csound(self):
        return '\n'.join([i.to_csound() for i in self.instr])

class Sco(Scrittore):
    def __init__(self,file):
        super().__init__(file)
        self.freqs=[]
        self.bandwidth=[]
        self.setUp(file)

    def setUp(self,file):
        super().setUp(file)
        self.instr.append(I1(1,float(self.data[0]),float(self.data[1]),int(self.data[3]),self.freq,int(self.data[5])*int(self.data[6])))
        self.calc_freq()
        for i in range(self.n_oct*self.flt_x_oct):
            self.instr.append(I91(91,float(self.data[0]),float(self.data[1]),self.freqs[i],self.bandwidth[i],i+2))
        self.instr.append(ScoScript(1000,float(self.data[0]),float(self.data[1])))

        self.l=Lettore(self.data[7],self.n_oct*self.flt_x_oct,int(self.data[8]))


    def to_csound(self):
        s=super().to_csound()
        l=self.l.to_csound()
        f="f100 0 [2^18] 11 1\n"
        return f+l+"\n"+s

    def calc_freq(self):
        up_freq=self.freq
        for i in range(self.n_oct):
            down_freq=self.freq*2**(i)
            if down_freq>10500:
                up_freq=20000.0
            else:
                up_freq=self.freq*2**(i+1)
            step=(up_freq-down_freq)/self.flt_x_oct
            for f in range(self.flt_x_oct):
                self.freqs.append(down_freq)
                self.bandwidth.append(step*2.0)
                down_freq+=step


class Orc(Scrittore):
    def __init__(self,file):
        self.instr=[]
        self.setUp(file)

    def setUp(self,file):
        super().setUp(file)
        self.instr.append(Head())
        self.instr.append(Instr1_gbuzz())
        self.instr.append(Instr91())
        self.instr.append(Instr1000(self.n_oct,self.flt_x_oct,self.data[7]))

    def to_csound(self):
        return super().to_csound()
