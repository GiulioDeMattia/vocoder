import pdb

class ScoScript:
    def __init__(self,number,at,dur):
        self.num=number
        self.at=at
        self.dur=dur

    def to_csound(self):
        return "i%d\t%.8f\t%12.8f\t"%(self.num,self.at,self.dur)

class I1(ScoScript):
    def __init__(self,number,at,dur,amp,freq,num_flt):
        super().__init__(number,at,dur)
        self.amp=amp
        self.freq=freq
        self.num_flt=num_flt


    def to_csound(self):
        string=super().to_csound()
        string2="%d\t%.4f\t%d"%(self.amp,self.freq,self.num_flt)
        return string+string2


class I91(ScoScript):
    def __init__(self,number,at,dur,frequenza,banda,channel):
        super().__init__(number,at,dur)
        self.frequenza=frequenza
        self.banda=banda
        self.canale=channel

    def to_csound(self):
        string=super().to_csound()
        string2="%.4f\t%.5f\t%d"%(self.frequenza,self.banda,self.canale)
        return string+string2

class OrcScript:
    def __init__(self):
        self.string=[]

    def to_csound(self):
        return self.string

class Head(OrcScript):
    def __init__(self):
        super().__init__()
        self.string="""
sr=48000
ksmps=3
0dbfs=1
nchnls=1
zakinit 1000,1000

"""
    def to_csound(self):
        return super().to_csound()



class Instr1_gbuzz(OrcScript):
    def __init__(self):
        self.string="""
	instr 1
iamp=ampdbfs(p4)
ifreq=p5
knh=10*p6
klh=1
as	gbuzz iamp, ifreq,knh,klh,1,100
zaw as,0
	endin
"""
    def to_csound(self):
        return super().to_csound()



class Instr1_noise(OrcScript):
    def __init__(self):
        self.string="""
	instr 1
iamp=ampdbfs(p4)
ifreq=p5
knh=10*p6
klh=1
as	gbuzz iamp, ifreq,knh,klh,1,100
zaw as,0
	endin

"""
    def to_csound(self):
        return super().to_csound()



class Instr91(OrcScript):
    def __init__(self):
        self.string="""
	instr 91
asrl zar 0
ifreq=p4
ibandwidth=p5
ifn_channel=p6
kndx line 0,p3,1
kenv tablei kndx,ifn_channel,1
as butterbp asrl,ifreq,ibandwidth
zaw as*kenv,ifn_channel
    endin\n\n"""

    def to_csound(self):
        return super().to_csound()


class Instr1000(OrcScript):
    def __init__(self,n_oct,flt_x_oct,data):
        super().__init__()
        self.data=data
        self.n_oct=n_oct
        self.flt_x_oct=flt_x_oct
        self.str_a=[]
        self.setUp()

    def setUp(self):
        for i in range(self.n_oct*self.flt_x_oct):
            self.str_a.append("a%d"%(i))
        s0="\tinstr 1000\n"
        s1="acompare zar 0\n"
        s2=self.str_zar()
        s3=self.str_out()
        s4="\n\tendin\n"
        self.string=s0+s1+s2+s3+s4

    def to_csound(self):
        return super().to_csound()

    def str_zar(self):
        return '\n'.join(["%s zar (%d+2)"%(self.str_a[i],i) for i in range(len(self.str_a))])

    def str_out(self):
        s1="\nasum ="
        s2='+'.join([i for i in self.str_a])
        s3="\naout balance asum, acompare\n  out aout"
        return s1+s2+s3
