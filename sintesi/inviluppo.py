from coordinata import Coordinata
import pdb

class Inviluppo:
    def __init__(self,number,tipology):
        self.num=number
        self.coordinate = []
        self.storage_locations=[]
        self.tip=tipology

    def add_coord(self, t, a):
        self.coordinate.append(Coordinata(t, a))

    def to_csound(self,size):
#        pdb.set_trace()
#        nino=self.testare()
        t = size/len(self.coordinate)
        self.storages(t)
        self.clean_it()
        string2=self.write_it()
        string1= "f%d 0 %d %d"%(self.num,size,self.tip)
        string=string1+string2+"0 1 0"
        return string

    def storages(self,t):
        step=int(t)
        acc_float=t
        acc_int=step
        count=0
        acc=acc_float-int(acc_int)
        for c in self.coordinate:
            count+=acc
            c.tempo=(step+int(count))
            if count > 1.0:
                count-=1.0

    def clean_it(self):
#        pdb.set_trace()
        j=0
        for i in range(len(self.coordinate)):
            if (i-j == 0):
                continue
            else:
                if self.coordinate[i-j].ampiezza == self.coordinate[i-j-1].ampiezza:
                    self.coordinate[i-j-1].tempo+=self.coordinate[i-j].tempo
                    del self.coordinate[i-j]
                    j+=1

    def write_it(self):
        return ''.join([c.to_csound() for c in self.coordinate])







#################-deprecato-#########
def testare(self):
    how_much=0
    for c in self.coordinate:
        how_much+=c.tempo
        return how_much

   #################-deprecato-#########
