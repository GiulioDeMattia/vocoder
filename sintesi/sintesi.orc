
sr=48000
ksmps=3
0dbfs=1
nchnls=1
zakinit 1000,1000



	instr 1
iamp=ampdbfs(p4)
ifreq=p5
knh=10*p6
klh=1
as	gbuzz iamp, ifreq,knh,klh,1,100
zaw as,0
	endin


	instr 91
asrl zar 0
ifreq=p4
ibandwidth=p5
ifn_channel=p6
kndx line 0,p3,1
kenv tablei kndx,ifn_channel,1
as butterbp asrl,ifreq,ibandwidth
zaw as*kenv,ifn_channel
    endin


	instr 1000
acompare zar 0
a0 zar (0+2)
a1 zar (1+2)
a2 zar (2+2)
a3 zar (3+2)
a4 zar (4+2)
a5 zar (5+2)
a6 zar (6+2)
a7 zar (7+2)
a8 zar (8+2)
a9 zar (9+2)
a10 zar (10+2)
a11 zar (11+2)
a12 zar (12+2)
a13 zar (13+2)
a14 zar (14+2)
a15 zar (15+2)
a16 zar (16+2)
a17 zar (17+2)
a18 zar (18+2)
a19 zar (19+2)
a20 zar (20+2)
a21 zar (21+2)
a22 zar (22+2)
a23 zar (23+2)
a24 zar (24+2)
a25 zar (25+2)
a26 zar (26+2)
a27 zar (27+2)
a28 zar (28+2)
a29 zar (29+2)
a30 zar (30+2)
a31 zar (31+2)
a32 zar (32+2)
a33 zar (33+2)
a34 zar (34+2)
a35 zar (35+2)
a36 zar (36+2)
a37 zar (37+2)
a38 zar (38+2)
a39 zar (39+2)
a40 zar (40+2)
a41 zar (41+2)
a42 zar (42+2)
a43 zar (43+2)
a44 zar (44+2)
a45 zar (45+2)
a46 zar (46+2)
a47 zar (47+2)
asum =a0+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14+a15+a16+a17+a18+a19+a20+a21+a22+a23+a24+a25+a26+a27+a28+a29+a30+a31+a32+a33+a34+a35+a36+a37+a38+a39+a40+a41+a42+a43+a44+a45+a46+a47
aout balance asum, acompare
  out aout
	endin
