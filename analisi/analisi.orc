
sr=48000
ksmps=3
0dbfs=1
zakinit 1,100

  	instr 1
ifile=p4
as,athrow	diskin2	ifile,1
zaw as,0
   	endin


   	instr 91
asrl zar 0
ifreq=p4
ibandwidth=p5
ichannel=p6
as butterbp asrl, ifreq, ibandwidth
aenvfoll follow	as,0.01
kenvfoll downsamp aenvfoll
   zkw kenvfoll, ichannel
    endin


	instr 1000
k0 zkr (0+2)
k1 zkr (1+2)
k2 zkr (2+2)
k3 zkr (3+2)
k4 zkr (4+2)
k5 zkr (5+2)
k6 zkr (6+2)
k7 zkr (7+2)
k8 zkr (8+2)
k9 zkr (9+2)
k10 zkr (10+2)
k11 zkr (11+2)
k12 zkr (12+2)
k13 zkr (13+2)
k14 zkr (14+2)
k15 zkr (15+2)
k16 zkr (16+2)
k17 zkr (17+2)
k18 zkr (18+2)
k19 zkr (19+2)
k20 zkr (20+2)
k21 zkr (21+2)
k22 zkr (22+2)
k23 zkr (23+2)
k24 zkr (24+2)
k25 zkr (25+2)
k26 zkr (26+2)
k27 zkr (27+2)
k28 zkr (28+2)
k29 zkr (29+2)
k30 zkr (30+2)
k31 zkr (31+2)
k32 zkr (32+2)
k33 zkr (33+2)
k34 zkr (34+2)
k35 zkr (35+2)
k36 zkr (36+2)
k37 zkr (37+2)
k38 zkr (38+2)
k39 zkr (39+2)
k40 zkr (40+2)
k41 zkr (41+2)
k42 zkr (42+2)
k43 zkr (43+2)
k44 zkr (44+2)
k45 zkr (45+2)
k46 zkr (46+2)
k47 zkr (47+2)
know times

fprintks "env.data","%.4f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f\n",know,	k0,	k1,	k2,	k3,	k4,	k5,	k6,	k7,	k8,	k9,	k10,	k11,	k12,	k13,	k14,	k15,	k16,	k17,	k18,	k19,	k20,	k21,	k22,	k23,	k24,	k25,	k26,	k27,	k28,	k29,	k30,	k31,	k32,	k33,	k34,	k35,	k36,	k37,	k38,	k39,	k40,	k41,	k42,	k43,	k44,	k45,	k46,	k47
	endin
