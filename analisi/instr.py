import pdb

class Instrsco:
    def __init__(self,number,at,dur):
        self.num=number
        self.at=at
        self.dur=dur

    def to_csound(self):
        return "i%d\t%.8f\t%12.8f\t"%(self.num,self.at,self.dur)

class I1(Instrsco):
    def __init__(self,number,at,dur,nome):
        super().__init__(number,at,dur)
        self.nome=nome

    def to_csound(self):
        string=super().to_csound()
        string2="%s"%(self.nome)
        return string+string2

class I91(Instrsco):
    def __init__(self,number,at,dur,frequenza,banda,channel):
        super().__init__(number,at,dur)
        self.frequenza=frequenza
        self.banda=banda
        self.canale=channel

    def to_csound(self):
        string=super().to_csound()
        string2="%.4f\t%.5f\t%d"%(self.frequenza,self.banda,self.canale)
        return string+string2

class Instrorc_Head:
    def __init__(self):
        self.string="""
sr=48000
ksmps=3
0dbfs=1
zakinit 1,100

  	instr 1
ifile=p4
as,athrow	diskin2	ifile,1
zaw as,0
   	endin


   	instr 91
asrl zar 0
ifreq=p4
ibandwidth=p5
ichannel=p6
as butterbp asrl, ifreq, ibandwidth
aenvfoll follow	as,0.01
kenvfoll downsamp aenvfoll
   zkw kenvfoll, ichannel
    endin\n\n"""

    def to_csound(self):
        return self.string


class Instrorc:
    def __init__(self,n_oct,flt_x_oct,data):
        self.data=data
        self.n_oct=n_oct
        self.flt_x_oct=flt_x_oct
        self.str_k=[]
        self.str_fprintks=[]
        self.setUp()

    def setUp(self):
        self.str_fprintks.append("%.4f")
        self.str_k.append("know")
        for i in range(self.n_oct*self.flt_x_oct):
            self.str_k.append("k%d"%(i))
            self.str_fprintks.append("%.8f")

    def to_csound(self):
        s0="\tinstr 1000\n"
        s1=self.str_zkr()
        s2=self.str_know()
        s3=self.str_fprint()
        s4="\n\tendin\n"
        return s0+s1+s2+s3+s4


    def str_zkr(self):
        return '\n'.join(["%s zkr (%d+2)"%(self.str_k[i+1],i) for i in range(len(self.str_k)-1)])


    def str_know(self):
        return "\nknow times\n\n"



    def str_fprint(self):

        s1="fprintks %s,\""%(self.data)
        s2=' '.join([i for i in self.str_fprintks])
        s3="\\n\","
        s4=',\t'.join([i for i in self.str_k])
        return s1+s2+s3+s4
